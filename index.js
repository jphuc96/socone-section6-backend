const app = require('./app')

const address = '0.0.0.0'
const port = 3000


// listen at 0.0.0.0:3000
app.listen(port, address, () => {
  console.log(`Server running at http://${address}:${port}`)
})