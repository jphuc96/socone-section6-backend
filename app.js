const express = require('express')
const app = express()

app.get('/api/hello', (req, res) => {
  console.log('GET /api/hello')

  res.send({ message: 'Hello World!' })
})

module.exports = app